const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const product = new Schema({
	staffName: {type: String, required: true},
	amount: {type: String, required: false},
	// action: {type: String, required: false},
	arrivingDate: {type: String, required: true},
	characteristics: [
		{
			weight: String, required: false,
			length: String, required: false,
			width: String, required: false
		},
	]

});

module.exports = mongoose.model('product', product);