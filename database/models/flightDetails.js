const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const flightDetails = new Schema({
    departureDate: {type: Date, required: false},
    departureFlightNumber : {type: String, required: false},
    arrivingDate : {type: Date, required: false},
    arrivingFlightNumber : {type: String, required: false},
    travelForBusiness : {type: Boolean, required: false},
    companions : [{
        companionName : {type: String, required: false},
        typeCompanion  : {type: String, required: false}
    }],
    extraBedroom : {type: Boolean, required: false},
    status:{type:String, required: false}
});

module.exports = mongoose.model('flightDetails', flightDetails);

