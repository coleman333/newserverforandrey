const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const additionalUserInfo = new Schema({
    perfectWeekend: [{type: String, required: false}],
    perfectToLiveIn: [{type: String, required: false}],
    cuisine: [{type: String,required: false}],
    poisonedProducts:[{type: String,required: false}],
    sleepingHabits: {type: String,required: false},
    alcoholAttitude: {type: String,required: false},
    smockingAttitude: {type: String,required: false},
    musicAttitude:[{type: String, required: false}]
});

module.exports = mongoose.model('AdditionalUserInfo', additionalUserInfo);

