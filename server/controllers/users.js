const passport = require('passport');
const path = require('path');
const multer = require('multer');
const ObjectId = require('mongoose').Types.ObjectId;
let userModel = require('../../database/models/userModel');
let productModel = require('../../database/models/product');
let additionalUserInfoModel = require('../../database/models/additionalUserInfo');
let flightDetailsModel = require('../../database/models/flightDetails');
const _ = require('lodash');
const moment = require('moment');
const jsonwebtoken = require('jsonwebtoken');

const testImg = '../../images/1.3.jpg';

const fs = require('fs');

module.exports.upload = multer({
	storage: multer.diskStorage({
		destination: function (req, file, cb) {
			const absolutePath = path.join(__dirname, '../../images/');
			cb(null, absolutePath);
			console.log('this is multer');
		},
		filename: function (req, file, cb) {
			cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
		}
	})
});

module.exports.registration = async function (req, res, next) {
	try {
		// console.dir(req.body);
		// console.dir(req.file);
		const util = require('util');
		const userData = req.body;
		console.log('=====.=====>',req.body)
		userData.email = _.toLower(userData.email);
		// const header = { "alg": "HS256", "typ": "JWT"};
		// const unsignedToken = base64urlEncode(header) + '.' + base64urlEncode(payload)
		// const signature = HMAC-SHA256(unsignedToken, SECRET_KEY)
		// const token = encodeBase64Url(header) + '.' + encodeBase64Url(payload) + '.' + encodeBase64Url(signature)

		// , 'default.jpg'
		if(req.file) {
            userData.avatar = `/images/${_.get(req.file, 'filename')}`;
        }
		// if(userData.role==='tourist'){
		//
		// }
		// userData.role = 'tourist';
		// // userData.role = 'admin';

		const user = await util.promisify(userModel.create).call(userModel, userData);

		const payload = {userId: user._id, role: user.role};
		const randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
		user.secret = randLetter + Date.now();
		user.authToken = jsonwebtoken.sign(payload, user.secret);
		await util.promisify(user.save).call(user);

		// await util.promisify(req.logIn).call(req, user);

		res.json({
			user: _.omit(user.toJSON(), 'password', 'secret'),
			token: user.authToken
		});
	} catch (ex) {
		next(ex)
	}
};

module.exports.authenticate = async function (req, res, next) {
	try {
		const token = req.get('Authorization');

		if (!token) {
			const error = new Error('Permission denied!');
			error.status = 403;
			throw error;
		}

		const user = await userModel.findOne({authToken: token})
			.select('-password -secret')
			.exec();

		if (!user) {
			const error = new Error('Permission denied!');
			error.status = 403;
			throw error;
		}

		req.user = user;
		next()
	} catch (ex) {
		next(ex);
	}
};

module.exports.checkPermissions = async function (req, res, next) {
	try {
		const {userId} = req.params;
		const authUser = req.user;

		if (authUser.role !== 'admin' && authUser._id.toString() !== userId) {
			throw new Error('You can\'t edit other user.')
		}

		next()
	} catch (ex) {
		next(ex);
	}
};

module.exports.login = async function (req, res, next) {
	try {
		const util = require('util');
		let {email, password} = req.body;
		email = _.toLower(email);
		// console.log('this is server login', email,password );

		const user = await userModel.findOne({email: email}).exec();

		if (!user) {
			res.status(404);
			return res.end('No user found');
		}

		const isMatch = await util.promisify(user.comparePassword).call(user, password);

		if (!isMatch) {
			res.status(403);
			return res.end('Incorrect password.');
		}

		const payload = {userId: user._id, role: user.role};
		const randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
		user.secret = randLetter + Date.now();
		user.authToken = jsonwebtoken.sign(payload, user.secret);
		await util.promisify(user.save).call(user);

		return res.json({
			user: _.omit(user.toJSON(), ['secret', 'password']),
			token: user.authToken
		})

	} catch (e) {
		next(e)
	}
};

module.exports.checkEmail = function (req, res, next) {
	const {email} = req.body;
	// console.log('this is server user', email);
	userModel.findOne({email: email}, function (err, user) {
		if (err) {
			// console.error(err);
			return next(err);
		}

		return res.json({isEmail: Boolean(user)})
	});
};

module.exports.createPIIfNotExists = async function (req, res, next) {
	try {
		const {userId} = req.params;

		const personalInfo = _.pick(req.body,[
			'perfectWeekend',
			'perfectToLiveIn',
			'cuisine',
			'poisonedProducts',
			'sleepingHabits',
			'alcoholAttitude',
			'smockingAttitude',
			'musicAttitude'
		]);

		if (!userId) {
			throw new Error('You should specify userId');
		}
		const util = require('util');

		const user = await userModel.findById(userId).populate('info').exec();

		if (!user) {
			throw new Error('Any user was found.');
		}

		let info = user.info;

		if (info) {
			return next()
		}

		info = await util.promisify(additionalUserInfoModel.create).call(additionalUserInfoModel, personalInfo);
		await util.promisify(user.update).call(user, {$set: {info: info._id}});

		return next();
	} catch (e) {
		next(e)
	}
};

module.exports.createPersonalInfo = async function (req, res, next) {
		res.end('success');
};

module.exports.getPersonalInfo = async function (req, res, next) {
	try {
		const {userId} = req.params;
		const user = await userModel.findById(userId)
			.select('-password -secret')
			.populate('info')
			.exec();

		res.json({user});
	} catch (e) {
		next(e)
	}
};

module.exports.editPersonalInfo = async function (req, res, next) {
	try {
		// console.log(req.params);
		//here must be check of the token
		const {userId} = req.params;
		const personalInfo = req.body;
		console.log('==>', personalInfo);
		if (!userId) {
			throw new Error('You should specify userId');
		}
		const util = require('util');

		const user = await userModel.findById(userId).populate('info').exec();
		// console.log('this is user from put', user);
		if (!user) {
			throw new Error('Any user was found.');
		}
        if(req.file) {
            user.avatar = `/images/${_.get(req.file, 'filename')}`;
			await util.promisify(user.save).call(user);
        }
		let info = user.info;

		if (!info) {
			throw new Error('No info found.')
		}

        if (personalInfo.password) {
            user.password = personalInfo.password;
            await util.promisify(user.save).call(user)
        }

        if(personalInfo.birthDay){
        	console.log('123123452346234563456');
			user.birthDay = personalInfo.birthDay;
			await util.promisify(user.save).call(user);
		}

		await util.promisify(user.update).call(user,{$set:_.omit(personalInfo,['password'])});
		// await util.promisify(user.update).call(user,{$set:_.omit(personalInfo,['password'])});

		// await util.promisify(user.update).call(user, {$set: personalInfo});
		await util.promisify(info.update).call(info, {$set: personalInfo});

		res.end('success');
	} catch (e) {
		next(e)
	}
};

module.exports.deletePersonalInfo = function (req, res, next) {

}

module.exports.getAllUsers = function (req, res, next) {
	try {
		const {skip, limit, filter} = req.params;
		// const {skip, limit, filter} = req.query;

		userModel.find()
			.select('-password -secret')
			.sort({createdAt: filter})
			.limit(parseInt(limit))
			.skip(parseInt(skip))
			.exec(function (err, users) {
				console.log(users);
				userModel.count().exec(function (err, count) {
					if (err) {
						return next(err);
					}
					users = {
						users: users,
						count: count
					};
					res.json(users);
				})
			})
	} catch (e) {
		next(e)
	}
};

module.exports.getAllUserByDate = async function (req, res, next) {
	try {
		const {month,year,
			// offset, limit,
			order = 'asc'} = req.query;
		const monthMoment = moment().set({month: +month - 1, year: +year});
		// const yearMoment = moment().set({year: +year - 1});
		const startOfMonth = monthMoment.startOf('month').toDate();
		const endOfMonth = monthMoment.endOf('month').toDate();
		// const endOfYear = yearMoment.endOf('year').toDate();
		// const startOfYear = yearMoment.endOf('year').toDate();

		const flightDetailsOfInterest = await flightDetailsModel
			.find({
				$or: [
					{departureDate: {$gte: startOfMonth, $lte: endOfMonth}},
					{arrivingDate: {$gte: startOfMonth, $lte: endOfMonth}},
				]
				// $and:[
                //     {departureDate: {$gte: startOfYear, $lte: endOfYear}},
                //     {arrivingDate: {$gte: startOfYear, $lte: endOfYear}},
				// ]
			})
			.select('_id')
			.exec();

		const users = await userModel.find({flightDetails: {$in: _.map(flightDetailsOfInterest,'_id')}})
			.select('-password -secret')
			.sort({createdAt: order})
			.populate({path:'flightDetails',match:{			$or: [
						{departureDate: {$gte: startOfMonth, $lte: endOfMonth}},
						{arrivingDate: {$gte: startOfMonth, $lte: endOfMonth}},
					]}})
			// .limit(+limit)
			// .skip(+offset)
			.exec();

		const count = await userModel.count({flightDetails: {$in: flightDetailsOfInterest}}).exec();
		console.log("=====>===>",users);
		res.json({
			users: users,
			count: count
		});
	} catch (e) {
		next(e)
	}
};

module.exports.changePassword = async function (req, res, next) {
	try {
		const {email} = req.body;
		const util = require('util');
		const transporter = require('../mail');

		const user = await userModel.findOne({email}).exec();

		if (!user) {
			throw new Error('User not found.')
		}

		const randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
		const newPassword = randLetter + Date.now();

		user.password = newPassword;

		await util.promisify(user.save).call(user);

		const data = {
			to: email,
			subject: 'forgot password',
			text: `Your temporary password will be ${newPassword}`,
		};

		await util.promisify(transporter.sendMail).call(transporter, data);

		res.end('success');
	} catch (e) {
		next(e)
	}
};

module.exports.createFlightDetails = async function (req, res, next) {
	try {
		const {userId} = req.params;
		const flightDetails = req.body;

		if (!userId) {
			throw new Error('You should specify userId');
		}
		const util = require('util');

		const user = await userModel.findById(userId).populate('flightDetails').exec();

		if (!user) {
			throw new Error('Any user was found.');
		}
		// moment(flightDetails.arrivingDate);	//increase on 1

		console.log('fd',flightDetails);
		let newElem = await util.promisify(flightDetailsModel.create).call(flightDetailsModel, flightDetails);

		await util.promisify(user.update).call(user, {$push: {flightDetails: newElem._id}});

		res.end('success');
	} catch (e) {
		next(e)
	}
};

module.exports.getFlightDetails = async function (req, res, next) {
	try {
		const {userId, ticketId} = req.params;

		const user = await userModel.findOne({_id: ObjectId(userId)})
			.select('-password -secret')
			.populate({
				path: 'flightDetails',
				match: {_id: ObjectId(ticketId)}
			})
			.exec();

		res.json({user});
	} catch (e) {
		next(e)
	}
};

module.exports.getAllFlightDetails = async function (req, res, next) {
	try {
		const {userId} = req.params;
		const user = await userModel.findById(userId)
			.select('-password -secret')
			.populate('flightDetails')
			.exec();

		res.json({user});
	} catch (e) {
		next(e)
	}
};

module.exports.editFlightDetails = async function (req, res, next) {
	try {
		const {userId, ticketId} = req.params;
		const flightDetailsData = req.body;

		if (!userId) {
			throw new Error('You should specify userId');
		}

		const util = require('util');

		const user = await userModel.findOne({_id: userId, flightDetails: ticketId}).populate('flightDetails').exec();

		if (!user) {
			throw new Error('No any user was found.');
		}

		const user2 =await util.promisify(flightDetailsModel.findOneAndUpdate).call(flightDetailsModel, {_id: ticketId}, {$set: flightDetailsData});
		console.log('=============>',user2);
		res.end('success');
	} catch (e) {
		next(e)
	}
};

// module.exports.editUserByAdmin = async function (req, res, next) {
//     try {
//         const {userId, ticketId} = req.params;
//         const flightDetailsData = req.body;
//
//         if (!userId) {
//             throw new Error('You should specify userId');
//         }
//
//         const util = require('util');
//
//         const user = await userModel.findOne({_id: userId, flightDetails: ticketId}).populate('flightDetails').exec();
//
//         if (!user) {
//             throw new Error('No any user was found.');
//         }
//
//         const user2 =await util.promisify(flightDetailsModel.findOneAndUpdate).call(flightDetailsModel, {_id: ticketId}, {$set: flightDetailsData});
//         console.log('=============>',user2);
//         res.end('success');
//     } catch (e) {
//         next(e)
//     }
// };

module.exports.logout = async function (req, res, next) {
	try {
		const util = require('util');
		const {userId} = req.params;
		const user = await userModel.findById(userId).exec();
		user.authToken = null;
		await util.promisify(user.save).call(user);
		res.end();
	} catch (e) {
		next(e)
	}
};


