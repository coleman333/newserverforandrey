const nodemailer = require('nodemailer');
const config = require('./config');

const transporter = nodemailer.createTransport(config, {from: 'invitemeservice@gmail.com',});

module.exports = transporter;