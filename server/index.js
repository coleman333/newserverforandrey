const cluster = require('cluster');
const path = require('path');
const os = require('os');
const logger = require('./config/logger');

const nodeEnv = process.env.NODE_ENV;
const numCPUs = nodeEnv === 'production' ? os.cpus().length : 1;  // start only 1 instance for development mode

if (cluster.isMaster) {
    logger.debug(`Master ${process.pid} is running`);
    // TODO: configure for https when available ssl certs
    cluster.setupMaster({
        exec: path.join(__dirname, './worker.js'),
        // args: ['--use', 'http']
        // silent: true
    });

    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('message', (worker, message)=> {
        switch (message.type) {
            case 'shutdown':
            {
                cluster.disconnect(()=> {
                    process.exit(message.code);
                })
            }
                break;
            default:
            {
                console.dir(message);
            }
        }
    });

    cluster.on('disconnect', worker=> {
        logger.debug('Worker %d disconnected', worker.id);
    });
    cluster.on('exit', worker=> {
        // Replace the dead worker
        if (worker.exitedAfterDisconnect) {
            logger.debug('Worker %d exited', worker.id);
        } else {
            logger.debug('Worker %d died :( and new one re-spawned', worker.id);
            cluster.fork();
        }
    });

    console.log('--------------------------');
    console.log('===> 😊  Starting Server . . .');
    console.log('===>  Environment: ' + nodeEnv);
}
