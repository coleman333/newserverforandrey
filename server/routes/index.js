// App is a function that requires store data and url to initialize and return the React-rendered html string - server-side rendering
const users = require('./users');
const auth = require('./auth');
const swaggerUi = require('swagger-ui-express');
//const yamlFile = require('../../swagger/routes.yaml');
const YAML = require('yamljs');
const swaggerDocumentAccount = YAML.load(require('path').join(__dirname,'../../swagger/routes.yaml'));

module.exports = (app) => {
    app.use('/api/users', users);
    app.use('/api/auth', auth);
    app.use('/doc/account', swaggerUi.serve, swaggerUi.setup(swaggerDocumentAccount));
    app.use('/doc/', swaggerUi.serve, swaggerUi.setup(swaggerDocumentAccount));


// error handler
// development error handler
// will print stacktrace

    app.use((err, req, res, next) => {
        console.error(err);
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: app.get('env') === 'development' ? err : {}
        });
    });
};
