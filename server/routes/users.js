const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/users');

// router.post('/registration', userCtrl.upload.single('avatar'), userCtrl.registration);
router.post('/', userCtrl.upload.single('avatar'), userCtrl.registration);
router.post('/', userCtrl.upload.single('avatar'), userCtrl.registration);
// router.get('/:userId',userCtrl.authenticate,userCtrl.checkPermissions,userCtrl.getUser);// get single user
// router.put('/:userId',userCtrl.registration);// edit single user
router.post('/check-email', userCtrl.checkEmail);

router.post('/change-password',userCtrl.changePassword);
router.post('/login', userCtrl.login);
// router.delete('/:userId',userCtrl.registration);// delete single user
router.post('/:userId/pinfo', userCtrl.authenticate,userCtrl.checkPermissions,userCtrl.createPIIfNotExists, userCtrl.createPersonalInfo); //add personal info to user

router.get('/:userId/pinfo',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.getPersonalInfo);     //get personal info with user info
// router.put('/:userId/pinfo',userCtrl.authenticate,userCtrl.checkPermissions,userCtrl.upload.single('avatar'), userCtrl.editPersonalInfo);    //edit personal info with user info
router.put('/:userId/pinfo' ,userCtrl.upload.single('avatar'), userCtrl.createPIIfNotExists, userCtrl.editPersonalInfo);    //edit personal info with user info
router.post('/logout/:userId',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.logout);

router.post('/:userId/tickets',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.createFlightDetails);
router.get('/:userId/tickets/:ticketId',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.getFlightDetails); // fetch one
router.get('/:userId/tickets/',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.getAllFlightDetails); // fetch all
router.put('/:userId/tickets/:ticketId',userCtrl.authenticate,userCtrl.checkPermissions, userCtrl.editFlightDetails); // update one {key1:value, key2:value}
// router.delete('/:userId/pinfo', userCtrl.createPersonalInfo);
// router.delete('/:userId/tickets/:ticketId', userCtrl.createPersonalInfo); // delete one

router.get('/:skip/:limit/:filter',userCtrl.authenticate,userCtrl.getAllUsers);// fetch all users
router.get('/',userCtrl.getAllUserByDate);// fetch all users by date
router.put('/:userId/tickets/:ticketId/admin',userCtrl.editFlightDetails);// fetch all users by date
// router.get('/',userCtrl.authenticate,userCtrl.getAllUsers);// fetch all users




// router.patch('/users/:userId/tickets/:ticketId', userCtrl.createPersonalInfo); // update one {key:value}
module.exports = router;


