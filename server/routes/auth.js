const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', async (req, res, next) => {
	await passport.authenticate('facebook', async (error, user) => {
		if (error) {
			return next(error)
		}
		await require('util').promisify(req.login).call(req, user);
// res.end();
		return res.json({
			user:user,
			status:'ok'
		});
		// res.redirect(`http://localhost:4433/success`);
	})(req, res, next);
});

router.get('/instagram',passport.authenticate('instagram'));
// router.post('registration',)
router.get('/instagram/callback',async (req,res,next)=>{
	await passport.authenticate('instagram',async (error,user)=>{
		if(error){
			return next(error)
		}
        await require('util').promisify(req.login).call(req,user);
		return res.json({
			user:user,
			status:'ok'
		});
	})(req,res,next);
});

router
module.exports = router;