const util=require('util');
const transporter=require('../server/mail');

(async ()=>{

	try {
		const data = {
			to: 'yegorsmartit@gmail.com',
			subject: 'Message title',
			text: 'Plaintext version of the message',
		};

		await util.promisify(transporter.sendMail).call(transporter, data);

		console.log('Message sent.');
	} catch (e) {
		console.error(e);
		process.abort();
	}
})();